package com.tpc.example.aop.aspect;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.lang.reflect.Method;
import java.util.UUID;

/**
 * @author LiuDong 
 * @date 2021/4/23 0:32
 */
@Slf4j
@Aspect
@Component
@Scope("prototype")
public class FeignLogAop {

    private InterfaceDataRecordWithBlobs recordWithBlobs;

    /**
     * 功能描述: 定义切点切人feign.client包下所有类
     */
    @Pointcut("execution(* com.tpc.example.aop.feign.client..*.*(..)) ")
    public void executeService() {
    }

    /**
     * 功能描述: 在目标方法被调用之前做增强处理
     * @param joinPoint 切入点
     */
    @Before(value = "executeService()")
    public void beforeAdvice(JoinPoint joinPoint) {
        try {
            if (this.recordWithBlobs == null) {
                this.recordWithBlobs = new InterfaceDataRecordWithBlobs();
            }
            MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
            Method method = methodSignature.getMethod();
            PostMapping postMapping = method.getAnnotation(PostMapping.class);
            GetMapping getMapping = method.getAnnotation(GetMapping.class);
            RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
            if (postMapping == null && getMapping == null && requestMapping == null) {
                return;
            }
            String[] value = new String[]{};
            if (postMapping != null) {
                value = postMapping.value();
            }
            if (getMapping != null) {
                value = getMapping.value();
            }
            if (requestMapping != null) {
                value = requestMapping.value();
            }
            if (value.length <= 0) {
                return;
            }
            Object[] args = joinPoint.getArgs();
            this.recordWithBlobs.setInterfaceUrl(value[0]);
            this.recordWithBlobs.setInterfaceDataRecordId(UUID.randomUUID().toString().replace("-", ""));
            this.recordWithBlobs.setInterfaceParam(JSONObject.toJSONString(args));
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    /**
     * 功能描述: 在目标方法正常完成后做增强
     * @param obj 方法返回值
     */
    @AfterReturning(value = "executeService()", returning = "obj")
    public void afterAdviceReturn(Object obj) {
        try {
            if (obj != null) {
                JSONObject jsonObject = JSON.parseObject((String) obj);
                Integer status = jsonObject.getInteger("Code") != null ? jsonObject.getInteger("Code") : jsonObject.getInteger("status");
                this.recordWithBlobs.setInterfaceFlag(status);
                this.recordWithBlobs.setInterfaceResult(JSONObject.toJSONString(obj));
                // TODO 存入db
                log.info("{}", this.recordWithBlobs);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }
}
