package com.tpc.example.aop.feign.client;

import com.tpc.example.aop.feign.config.BasicAuthFeignConfiguration;
import com.tpc.example.aop.interceptor.restful.BookVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author LiuDong
 */
@FeignClient(url = "${rpc.url.book}", name = "bookFeignClient", contextId = "book", configuration = BasicAuthFeignConfiguration.class)
public interface BookService {

    /**
     * 查询book信息
     * @param name book name
     * @return book信息
     */
    @GetMapping(value = "/{name}")
    BookVo queryBookInfo(@PathVariable("name") String name);
}
