package com.tpc.example.aop.aspect;

import lombok.Data;

/**
 * @author LiuDong
 */
@Data
public class InterfaceDataRecordWithBlobs {

    private Object interfaceUrl;
    private String interfaceDataRecordId;
    private String interfaceParam;
    private Integer interfaceFlag;
    private String interfaceResult;
}
