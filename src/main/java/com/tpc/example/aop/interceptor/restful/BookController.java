package com.tpc.example.aop.interceptor.restful;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author LiuDong
 */
@RestController
@RequestMapping("/book")
public class BookController {

    @GetMapping(value = "/{name}")
    public BookVo queryBookInfo(@PathVariable("name") String name) {
        return BookVo.builder()
                .bookName(name)
                .build();
    }
}
