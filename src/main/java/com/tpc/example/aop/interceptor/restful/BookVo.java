package com.tpc.example.aop.interceptor.restful;

import lombok.Builder;
import lombok.Data;

/**
 * @author LiuDong
 */
@Data
@Builder
public class BookVo {

    private String bookName;
    private String intro;
}
