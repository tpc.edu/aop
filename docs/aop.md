[TOC]

# AOP概述

## 简介

首先先给出一段比较专业的术语（来自百度）：
> 在软件业，AOP为Aspect Oriented Programming的缩写，意为：面向切面编程，通过预编译方式和运行期动态代理实现程序功能的统一维护的一种技术。AOP是OOP的延续，是软件开发中的一个热点，也是Spring框架中的一个重要内容，是函数式编程的一种衍生范型。利用AOP可以对业务逻辑的各个部分进行隔离，从而使得业务逻辑各部分之间的耦合度降低，提高程序的可重用性，同时提高了开发的效率。

## AOP中的相关概念

`Aspect`（切面）： `Aspect`声明类似于 Java 中的类声明，在`Aspect`中会包含着一些`Pointcut`以及相应的`Advice`。
`Joint point`（连接点）：表示在程序中明确定义的点，典型的包括方法调用，对类成员的访问以及异常处理程序块的执行等等，它自身还可以嵌套其它`joint point`。
`Pointcut`（切点）：表示一组`joint point`，这些`joint point`或是通过逻辑关系组合起来，或是通过通配、正则表达式等方式集中起来，它定义了相应的`Advice`将要发生的地方。
`Advice`（增强）：`Advice`定义了在`Pointcut`里面定义的程序点具体要做的操作，它通过 before、after 和 around 来区别是在每个`joint point`之前、之后还是代替执行的代码。
`Target`（目标对象）：织入`Advice`的目标对象。
`Weaving`（织入）：将`Aspect`和其他对象连接起来, 并创建 Adviced object 的过程。

`AOP`中的`Joinpoint`可以有多种类型：构造方法调用，字段的设置和获取，方法的调用，方法的执行，异常的处理执行，类的初始化。也就是说在`AOP`的概念中我们可以在上面的这些`Joinpoint`上织入我们自定义的`Advice`，但是在Spring中却没有实现上面所有的`joinpoint`，确切的说，Spring只支持方法执行类型的`Joinpoint`。

### Pointcut

#### 匹配包

* 匹配`ProductService`类里的所有方法
`@Pointcut("within(com.tpc.service.ProductService)")`

* 匹配`com.tpc`包及子包下所有类的方法
`@Pointcut("within(com.tpc..*)")`

#### 匹配对象

* 匹配AOP对象的目标对象为指定类型的方法,即`LogService`的aop代理对象的方法
`@Pointcut("this(com.tpc.log.Loggable)")`

* 匹配实现`Loggable`接口的目标对象(而不是aop代理后的对象)的方法
`@Pointcut("target(com.tpc.log.Loggable)")`

* 匹配所有以`Service`结尾的bean里头的方法
`@Pointcut("bean(*Service)")`

#### 匹配参数`args()`

* 匹配任何以find开头而且只有一个`Long`参数的方法
`@Pointcut("execution(* ..find(Long))")`

* 匹配任何以find开头的而且第一个参数为`Long`型的方法
`@Pointcut("execution(* ..find(Long,..))")`

* 匹配任何只有一个`Long`参数的方法
`@Pointcut("within(com.tpc..*) && args(Long)")`

* 匹配第一个参数为`Long`型的方法
`@Pointcut("within(com.tpc..*) && args(Long,..)")`

#### 匹配注解

* 匹配方法标注有`AdminOnly`的注解的方法
`@Pointcut("@annotation(com.tpc.anno.AdminOnly) && within(com.tpc..*)")`

* 匹配标注有`NeedSecured`的类底下的方法 //class级别
`@Pointcut("@within(com.tpc.anno.NeedSecured) && within(com.tpc..*)")`

* 匹配标注有`NeedSecured`的类及其子类的方法 //runtime级别

* 在spring context的环境下,二者没有区别
`@Pointcut("@target(com.tpc.anno.NeedSecured) && within(com.tpc..*)")`

* 匹配传入的参数类标注有`Repository`注解的方法
`@Pointcut("@args(com.tpc.anno.Repository) && within(com.tpc..*)")`

#### 匹配方法

* 匹配任何公共方法
`@Pointcut("execution(public * com.tpc.service..(..))")`

* 匹配`com.tpc`包及子包下`Service`类中无参方法
`@Pointcut("execution(* com.tpc..Service.())")`

* 匹配`com.tpc`包及子包下`Service`类中的任何只有一个参数的方法
`@Pointcut("execution(* com.tpc..Service.(*))")`

* 匹配`com.tpc`包及子包下任何类的任何方法
`@Pointcut("execution(* com.tpc...(..))")`

* 匹配`com.tpc`包及子包下返回值为`String`的任何方法
`@Pointcut("execution(String com.tpc...(..))")`

* 匹配异常
`execution(public * com.tpc.service..(..) throws java.lang.IllegalAccessException)`

### Advice类型(通知类型或增强类型)

* `before advice`, 在 join point 前被执行的 advice. 虽然 before advice 是在 join point 前被执行, 但是它并不能够阻止 join point 的执行, 除非发生了异常(即我们在 before advice 代码中, 不能人为地决定是否继续执行 join point 中的代码). `@Before`只需要指定切入点表达式即可
* `after return advice`, 在一个 join point 正常返回后执行的 advice. `@AfterReturning`除了指定切入点表达式后，还可以指定一个`returning`的返回值形参名,代表目标方法的返回值
* `after throwing advice`, 当一个 join point 抛出异常后执行的 advice. `@AfterThrowing`除了指定切入点表达式后，还可以指定一个`throwing`的返回值形参名,可以通过该形参名来访问目标方法中所抛出的异常对象
* `after(final) advice`, 无论一个 join point 是正常退出还是发生了异常, 都会被执行的 advice.`@After`可以指定一个切入点表达式
* `around advice`, 在 join point 前和 joint point 退出后都执行的 advice. 这个是最常用的 advice. `@Around`是最重要的通知类型,像事务,日志等都是环绕通知,注意编程中核心是一个`ProceedingJoinPoint`
* `introduction`，introduction可以为原有的对象增加新的属性和方法。

## 参考

* [优秀文档-AOP详解](https://blog.csdn.net/q982151756/article/details/80513340 "细说Spring——AOP详解（AOP概览）") 
* [参考文档-猿小源](https://juejin.cn/post/6844903936541851662 "Aop 切面 Feign 记录") 
