package com.tpc.example.aop.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author LiuDong
 * 跨域请求及预请求解决拦截器
 */
@Slf4j
@Order(468)
@Component
public class CorsFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        log.info("---------CorsFilter 请求拦截---------");

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Credentials", "true");
        response.setHeader("Access-Control-Allow-Methods", "PUT, POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "36000");
        response.setHeader("Access-Control-Allow-Headers",
                "Accept, System-Code, Service-Org, Access-Token, Content-Type, Content-Disposition, Cache-Control, Expires, Referer, User-Agent, No-Cache, Pragma, X-Requested-With, If-Modified-Since, Last-Modified, Authorization");
        response.setHeader("XDomainRequestAllowed", "1");
        // 使前端能够获得到后端在Response中自定义的Header,避免被前端请求框架过滤
        response.setHeader("Access-Control-Expose-Headers", "Access-Token, System-Code,Service-Org, Content-Disposition, Authorization");
        if (HttpMethod.OPTIONS.toString().equals(request.getMethod())) {
            log.info("---------CorsFilter 预请求:默认通过---------");
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            log.info("---------CorsFilter 正式请求:执行下一过滤器---------");
            filterChain.doFilter(request, response);
        }
    }

}
