# SpringBoot集成feign

## 要点
> 重点理解`AOP`、`Interceptor`及`Filter`的原理、使用场景、用法，区别及联系。
>
> 过滤器、拦截器都只能对`controller`的方法进行增强，都是通过对请求路径的放行或拦截来控制作用范围；不同的是，过滤器只能实现前置通知，而拦截器可以实现环绕通知。  
> AOP，eg: `@Advice`、`@ControllerAdvice`，可以对任意被代理对象的方法进行增强，可以通过正则匹配包、对象、参数、注解、方法等多种方式(`Pointcut`)来控制作用范围；并且有前置通知、后置通知、正常结束后置通知、环绕通知、异常通知等。

* [AOP概述](docs/aop.md)
* [Filter概述](docs/filter.md)
* [Interceptor概述](docs/interceptor.md)

## 参考
* [参考文档-aop](https://blog.csdn.net/q982151756/article/details/80513340 "细说Spring——AOP详解（AOP概览）")
* [源码-feign-demo](https://gitlab.com/tpc.edu/aop)
