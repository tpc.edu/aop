package com.tpc.example.aop.interceptor;

import com.tpc.example.aop.interceptor.annotation.BasicAuth;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.lang.Nullable;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * @author LiuDong
 */
public class BasicAuthInterceptor implements HandlerInterceptor {

    /**
     * token在 BasicAuthRequestInterceptor 中生成
     * this.headerValue = "Basic " + base64Encode((username + ":" + password).getBytes(charset));
     */
    private static final String BASIC_AUTH_TOKEN = "Basic dGVzdDp0ZXN0";

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        HandlerMethod handlerMethod = (HandlerMethod) handler;
        Method method = handlerMethod.getMethod();
        if (method.isAnnotationPresent(BasicAuth.class)) {
            BasicAuth userToken = method.getAnnotation(BasicAuth.class);
            if (userToken.required()) {
                // 执行认证
                String token = request.getHeader(HttpHeaders.AUTHORIZATION);
                if (token == null || !token.equals(BASIC_AUTH_TOKEN)) {
                    // TODO token错误
                    response.setStatus(HttpStatus.UNAUTHORIZED.value());
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
                           @Nullable ModelAndView modelAndView) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
                                @Nullable Exception ex) throws Exception {
    }

}
